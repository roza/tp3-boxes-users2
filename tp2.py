class BoxContentError(Exception):
    pass

class BoxTooFullError(Exception):
    pass

class Thing:
    def __init__(self, vol):
        self._volume = vol

    def volume(self):
        return self._volume

class Box:
    def __init__(self, cap=None):
        self._contents = []
        self._open = False
        self._capacity = cap

    def capacity(self):
        return self._capacity

    def set_capacity(self, cap):
        self._capacity = cap

    def is_open(self):
        return self._open

    def open(self):
        self._open = True

    def close(self):
        self._open = False

    def action_add(self, objet):
        if not self.has_room_for(objet):
            raise BoxTooFullError
        self.add(objet)

    def add(self, objet):
        if self._capacity is not None:
            self._capacity -= objet.volume()
        self._contents.append(objet)

    def __contains__(self, objet):
        return objet in self._contents

    def remove(self, objet):
        if objet not in self:
            raise BoxContentError
        self._contents.remove(objet)

    def has_room_for(self, objet):
        if self.capacity() is None:
            return True
        return self.capacity() >= objet.volume()

    def action_look(self):
        if self.is_open():
            enumeration_contenu = ", ".join(self._contents)
            return "La boite contient: " + enumeration_contenu + "."
        else:
            return "La boite est fermée."

    def set_user(self,user):
        self._user = user
    
    def get_user(self):
        return self._user
    